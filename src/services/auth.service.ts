import { Injectable } from '@angular/core';
import { UserModel } from 'src/models/user.model';
import { map, tap, retry, catchError } from 'rxjs/operators';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AlertService } from './alert.service';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  private readonly currentUser = 'currentuser';
  private readonly currentToken = 'token';

  public readonly redirectUrl: string = '/dashboard';

  constructor(private http: HttpClient, private alertService: AlertService) {}

  // Login user
  login(user: UserModel) {
    return this.http
      .post<UserModel>(
        `${environment.apiUrl}/users/login`,
        user,
        this.httpOptions
      )
      .pipe(
        tap(
          (data) => console.log(data),
          (error) => console.error(error)
        )
      )
      .subscribe({
        next: (response: any) => {
          this.saveCurrentUser(response.result.userid, response.result.token);
          this.alertService.success('Logged in');
          console.log('Logged in');
        },
        error: (message: any) => {
          console.log('error:', message);
          console.log('Wrong credentials');
          this.alertService.error('Invalid credentials');
          console.log(this.alertService);
          alert('Verkeerde inloggegevens');
        },
      });
  }

  private saveCurrentUser(userid: string, token: string): void {
    localStorage.setItem(this.currentUser, userid);
    localStorage.setItem(this.currentToken, token);
  }

  logout() {
    localStorage.removeItem(this.currentUser);
    localStorage.removeItem(this.currentToken);
    this.alertService.success('Logged out');
    console.log('Logged out');
  }

  isLoggedIn() {
    const loggedIn = localStorage.getItem('token');
    if (loggedIn !== null && loggedIn !== undefined) {
      return true;
    } else {
      return false;
    }
  }
}
