import { Injectable } from '@angular/core';
import {
  Observable,
  of,
  BehaviorSubject,
  observable,
  Subject,
  throwError,
} from 'rxjs';
import { map, tap, retry, catchError } from 'rxjs/operators';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';

import { environment } from '../environments/environment';
import { ExpenseModel } from '../models/expense.model';

@Injectable({
  providedIn: 'root',
})
export class ExpenseService {
  constructor(private http: HttpClient) {}

  getHttpOptions() {
    const token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token,
      }),
    };
    return httpOptions;
  }

  // Get expenses from the server
  getExpenses(): Observable<ExpenseModel[]> {
    const httpOptions = this.getHttpOptions();
    return this.http
      .get<ExpenseModel[]>(`${environment.apiUrl}/expenses`, httpOptions)
      .pipe(
        tap((_) => console.log('Fetched expenses')),
        catchError(this.handleError<ExpenseModel[]>('getExpenses', []))
      );
  }

  // Get expense by ID
  getExpense(id: string): Observable<ExpenseModel> {
    const httpOptions = this.getHttpOptions();
    return this.http
      .get<ExpenseModel>(`${environment.apiUrl}/expenses/${id}`, httpOptions)
      .pipe(
        tap((_) => console.log(`fetched expense id=${id}`)),
        tap(console.log),
        catchError(this.handleError<ExpenseModel>(`getExpense id=${id}`))
      );
  }

  // Post expense
  addExpense(expense: ExpenseModel): Observable<ExpenseModel> {
    const httpOptions = this.getHttpOptions();
    return this.http
      .post<ExpenseModel>(
        `${environment.apiUrl}/expenses`,
        expense,
        httpOptions
      )
      .pipe(
        tap((newExpense: ExpenseModel) =>
          console.log(`Added expense with id=${newExpense._id}`)
        ),
        catchError(this.handleError<ExpenseModel>('addExpense'))
      );
  }

  // Delete expense
  deleteExpense(expense: ExpenseModel): Observable<any> {
    const id = expense._id;
    const httpOptions = this.getHttpOptions();
    return this.http
      .delete(`${environment.apiUrl}/expenses/${id}`, httpOptions)
      .pipe(
        tap((_) => console.log(`Deleted expense id=${expense._id}`)),
        catchError(this.handleError<any>('deleteExpense'))
      );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      alert('Oeps, er ging iets mis');

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
