import { Injectable } from '@angular/core';
import { UserModel } from 'src/models/user.model';
import {
  Observable,
  of,
  BehaviorSubject,
  observable,
  Subject,
  throwError,
} from 'rxjs';
import { map, tap, retry, catchError } from 'rxjs/operators';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  getCurrentuser() {
    const currentuser = localStorage.getItem('currentuser');
    return currentuser;
  }

  getHttpOptions() {
    const token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token,
      }),
    };
    return httpOptions;
  }

  constructor(private http: HttpClient) {}

  // Get user by ID
  getUser(): Observable<UserModel> {
    let id;
    const userid = this.getCurrentuser();
    if (userid) {
      id = userid;
    } else {
      console.log('User niet ingelogd');
      return;
    }
    const httpOptions = this.getHttpOptions();
    return this.http
      .get<UserModel>(`${environment.apiUrl}/users/${id}`, httpOptions)
      .pipe(
        tap((_) => console.log(`fetched user id=${id}`)),
        tap(console.log),
        catchError(this.handleError<UserModel>(`getUser id=${id}`))
      )
      .pipe();
  }

  // Get users from the server
  getUsers(): Observable<UserModel[]> {
    const httpOptions = this.getHttpOptions();
    return this.http
      .get<UserModel[]>(`${environment.apiUrl}/users`, httpOptions)
      .pipe(
        tap((_) => console.log('Fetched users')),
        catchError(this.handleError<UserModel[]>('getUsers', []))
      );
  }

  // Post user
  addUser(user: UserModel): Observable<UserModel> {
    return this.http
      .post<UserModel>(`${environment.apiUrl}/users`, user)
      .pipe(
        tap(console.log),
        catchError(this.handleError<UserModel>('addUser'))
      );
  }

  // Put user
  updateUser(user: UserModel): Observable<any> {
    let id;
    const userid = this.getCurrentuser();
    if (userid) {
      id = userid;
    } else {
      console.log('User niet ingelogd');
      return;
    }
    const httpOptions = this.getHttpOptions();
    return this.http
      .put(`${environment.apiUrl}/users/${id}`, user, httpOptions)
      .pipe(
        tap((_) => console.log(`Updated user id=${user._id}`)),
        catchError(this.handleError<any>('updateUser'))
      );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      console.log(`${operation} failed: ${error.message}`);

      if (error.error.message == 'Password is not correct') {
        alert('Wachtwoord is niet correct');
      } else {
        alert('Oeps, er ging iets mis');
      }

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
