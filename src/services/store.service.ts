import { Injectable } from '@angular/core';
import {
  Observable,
  of,
  BehaviorSubject,
  observable,
  Subject,
  throwError,
} from 'rxjs';
import { map, tap, retry, catchError } from 'rxjs/operators';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';

import { environment } from '../environments/environment';
import { StoreModel } from '../models/store.model';

@Injectable({
  providedIn: 'root',
})
export class StoreService {
  constructor(private http: HttpClient) {}

  getHttpOptions() {
    const token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token,
      }),
    };
    return httpOptions;
  }

  // Get stores from the server
  getStores(): Observable<StoreModel[]> {
    return this.http.get<StoreModel[]>(`${environment.apiUrl}/stores`).pipe(
      tap((_) => console.log('Fetched stores')),
      catchError(this.handleError<StoreModel[]>('getStores', []))
    );
  }

  // Get store by ID
  getStore(id: string): Observable<StoreModel> {
    return this.http.get<StoreModel>(`${environment.apiUrl}/stores/${id}`).pipe(
      tap((_) => console.log(`fetched store id=${id}`)),
      tap(console.log),
      catchError(this.handleError<StoreModel>(`getStore id=${id}`))
    );
  }

  // Post store
  addStore(store: StoreModel): Observable<StoreModel> {
    const httpOptions = this.getHttpOptions();
    return this.http
      .post<StoreModel>(`${environment.apiUrl}/stores`, store, httpOptions)
      .pipe(
        tap((newStore: StoreModel) =>
          console.log(`Added store with id=${newStore._id}`)
        ),
        catchError(this.handleError<StoreModel>('addStore'))
      );
  }

  // Put store
  updateStore(store: StoreModel): Observable<any> {
    const id = store._id;
    const httpOptions = this.getHttpOptions();
    return this.http
      .put(`${environment.apiUrl}/stores/${id}`, store, httpOptions)
      .pipe(
        tap((_) => console.log(`Updated store id=${store._id}`)),
        catchError(this.handleError<any>('updateStore'))
      );
  }

  // Delete store
  deleteStore(store: StoreModel): Observable<any> {
    const id = store._id;
    const httpOptions = this.getHttpOptions();
    return this.http
      .delete(`${environment.apiUrl}/stores/${id}`, httpOptions)
      .pipe(
        tap((_) => console.log(`Deleted store id=${store._id}`)),
        catchError(this.handleError<any>('deleteStore'))
      );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      if (error.error.message == 'This store already exists') {
        alert('Deze winkel bestaat al');
      } else {
        alert('Oeps, er ging iets mis');
      }

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
