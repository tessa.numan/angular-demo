import { ExpenseService } from './expense.service';

describe('ExpenseService', () => {
  let service: ExpenseService;

  let httpSpy: any;

  beforeEach(() => {
    httpSpy = jasmine.createSpyObj('HttpClient', ['get', 'post']);

    service = new ExpenseService(httpSpy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
