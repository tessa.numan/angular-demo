import { TestBed } from '@angular/core/testing';

import { UserService } from './user.service';

describe('UserService', () => {
  let service: UserService;

  let httpSpy: any;

  beforeEach(() => {
    httpSpy = jasmine.createSpyObj('HttpClient', ['get', 'post']);

    service = new UserService(httpSpy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
