import { HttpErrorResponse } from '@angular/common/http';
import { findLast } from '@angular/compiler/src/directive_resolver';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { StoreModel } from 'src/models/store.model';
import { defer } from 'rxjs';

import { StoreService } from './store.service';

// Create async observable error that errors
//  after a JS engine turn
export function asyncError<T>(errorObject: any) {
  return defer(() => Promise.reject(errorObject));
}

describe('StoreService', () => {
  let service: StoreService;

  let httpSpy: any;

  const storeData1: StoreModel = {
    _id: '1',
    name: 'name1',
    emailAddress: 'name@hotmail.com',
    phoneNumber: '0612345678',
    address: {
      _id: '1',
      city: 'city1',
      street: 'street',
      houseNumber: '50',
      postalCode: '1234AB',
    },
  };

  const storeData2: StoreModel = {
    _id: '2',
    name: 'name2',
    emailAddress: 'name@hotmail.com',
    phoneNumber: '0612345678',
    address: {
      _id: '2',
      city: 'city2',
      street: 'street',
      houseNumber: '50',
      postalCode: '1234AB',
    },
  };

  const storeData3: StoreModel = {
    _id: '3',
    name: 'name3',
    emailAddress: 'name@hotmail.com',
    phoneNumber: '0612345678',
    address: {
      _id: '3',
      city: 'city3',
      street: 'street',
      houseNumber: '50',
      postalCode: '1234AB',
    },
  };

  const storeDataArray: StoreModel[] = [storeData1, storeData2];

  beforeEach(() => {
    httpSpy = jasmine.createSpyObj('HttpClient', [
      'get',
      'post',
      'put',
      'delete',
    ]);

    service = new StoreService(httpSpy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get stores', () => {
    httpSpy.get.and.returnValue(of(storeDataArray));

    const subs = service.getStores().subscribe((stores) => {
      expect(stores).toEqual(storeDataArray);
    });

    expect(httpSpy.get.calls.count()).toBe(1);
    subs.unsubscribe();
  });

  it('should get store by id', () => {
    httpSpy.get.and.returnValue(of(storeData1));

    const subs = service.getStore(storeData1._id).subscribe((store) => {
      expect(store.name).toEqual('name1');
    });

    expect(httpSpy.get.calls.count()).toBe(1);
    subs.unsubscribe();
  });

  it('should add store', () => {
    httpSpy.post.and.returnValue(of(storeData1));

    const subs = service.addStore(storeData1).subscribe((store) => {
      expect(store.name).toEqual('name1');
    });
    subs.unsubscribe();
  });

  it('should update store', () => {
    storeData3.name = 'name4';
    httpSpy.put.and.returnValue(of(storeData3));

    const subs = service.updateStore(storeData3).subscribe((store) => {
      expect(store.name).toEqual('name4');
    });
    subs.unsubscribe();
  });

  it('should delete store', () => {
    const message = { mes: 'Store is deleted' };
    httpSpy.delete.and.returnValue(of(message));

    const subs = service.deleteStore(storeData1).subscribe((m) => {
      expect(m.mes).toEqual(message.mes);
    });
    subs.unsubscribe();
  });
});
