import { Injectable } from '@angular/core';
import {
  Observable,
  of,
  BehaviorSubject,
  observable,
  Subject,
  throwError,
} from 'rxjs';
import { map, tap, retry, catchError } from 'rxjs/operators';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';

import { OverviewModel } from '../models/overview.model';
import { UserModel } from '../models/user.model';
import { environment } from '../environments/environment';
import { AuthService } from './auth.service';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root',
})
export class OverviewService {
  constructor(private http: HttpClient) {}

  getCurrentuser() {
    const currentuser = localStorage.getItem('currentuser');
    return currentuser;
  }

  getHttpOptions() {
    const token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token,
      }),
    };
    return httpOptions;
  }

  // Get overviews from the server
  getOverviews(): Observable<OverviewModel[]> {
    const httpOptions = this.getHttpOptions();
    const userid = this.getCurrentuser();
    return this.http
      .get<OverviewModel[]>(
        `${environment.apiUrl}/overviews/all/${userid}`,
        httpOptions
      )
      .pipe(
        tap((_) => console.log('Fetched overviews of user')),
        catchError(this.handleError<OverviewModel[]>('getOverviews', []))
      );
  }

  // Get overview by ID
  getOverview(id: string): Observable<OverviewModel> {
    const httpOptions = this.getHttpOptions();
    return this.http
      .get<OverviewModel>(`${environment.apiUrl}/overviews/${id}`, httpOptions)
      .pipe(
        tap((_) => console.log(`fetched overview id=${id}`)),
        tap(console.log),
        catchError(this.handleError<OverviewModel>(`getOverview id=${id}`))
      );
  }

  // Post overview
  addOverview(overview: OverviewModel): Observable<OverviewModel> {
    if (overview.userRefs) {
      console.log('userRefs: ', overview.userRefs);
    } else {
      console.log('User niet gevonden');
      return;
    }
    const httpOptions = this.getHttpOptions();
    return this.http
      .post<OverviewModel>(
        `${environment.apiUrl}/overviews`,
        overview,
        httpOptions
      )
      .pipe(
        tap((newOverview: OverviewModel) =>
          console.log(`Added overview with id=${newOverview._id}`)
        ),
        catchError(this.handleError<OverviewModel>('addOverview'))
      );
  }

  // Put overview
  updateOverview(overview: OverviewModel): Observable<any> {
    const userid = this.getCurrentuser();
    if (userid) {
      const idList = overview.userRefs;
      const result = idList.filter((f) => f._id === userid);
      if (result != null) {
        const id = overview._id;
        const httpOptions = this.getHttpOptions();
        return this.http
          .put(`${environment.apiUrl}/overviews/${id}`, overview, httpOptions)
          .pipe(
            tap((_) => console.log(`Updated overview id=${overview._id}`)),
            catchError(this.handleError<any>('updateOverview'))
          );
      } else {
        console.log('User heeft geen toegang');
        return;
      }
    } else {
      console.log('User niet ingelogd');
      return;
    }
  }

  // Put overview
  updateOverviewUsers(
    overview: OverviewModel,
    users: UserModel[]
  ): Observable<any> {
    const userid = this.getCurrentuser();
    if (userid) {
      const idList = overview.userRefs;
      const result = idList.filter((f) => f._id === userid);
      if (result != null) {
        overview.userRefs = users;
        const id = overview._id;
        const httpOptions = this.getHttpOptions();
        return this.http
          .put(`${environment.apiUrl}/overviews/${id}`, overview, httpOptions)
          .pipe(
            tap((_) => console.log(`Updated overview id=${overview._id}`)),
            catchError(this.handleError<any>('updateOverview'))
          );
      } else {
        console.log('User heeft geen toegang');
        return;
      }
    } else {
      console.log('User niet ingelogd');
      return;
    }
  }

  // Delete overview
  deleteOverview(overview: OverviewModel): Observable<any> {
    const userid = this.getCurrentuser();
    const id = overview._id;
    const httpOptions = this.getHttpOptions();
    return this.http
      .delete(`${environment.apiUrl}/overviews/${id}`, httpOptions)
      .pipe(
        tap((_) => console.log(`Deleted overview id=${overview._id}`)),
        catchError(this.handleError<any>('deleteOverview'))
      );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      if (error.error.message == 'This name already exists') {
        alert('Deze naam bestaat al');
      } else {
        alert('Oeps, er ging iets mis');
      }

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
