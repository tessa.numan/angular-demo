import { TestBed } from '@angular/core/testing';

import { OverviewService } from './overview.service';

describe('OverviewService', () => {
  let service: OverviewService;

  let httpSpy: any;

  beforeEach(() => {
    httpSpy = jasmine.createSpyObj('HttpClient', ['get', 'post']);

    service = new OverviewService(httpSpy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
