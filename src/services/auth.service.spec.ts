import { AuthService } from './auth.service';

describe('AuthService', () => {
  let service: AuthService;

  let httpSpy: any;
  let alertServiceSpy: any;
  let userServiceSpy: any;

  beforeEach(() => {
    alertServiceSpy = jasmine.createSpyObj('AlertService', [
      'error',
      'success',
    ]);

    httpSpy = jasmine.createSpyObj('HttpClient', ['get', 'post']);

    service = new AuthService(httpSpy, alertServiceSpy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
