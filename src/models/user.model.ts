import { AddressModel } from './address.model';

export class UserModel {
  _id: string;
  username: string;
  password: string;
  newPassword: string;
  emailAddress: string;
  phoneNumber: string;
  address: AddressModel;

  constructor(values = {}) {
    Object.assign(this, values)
  }
}
