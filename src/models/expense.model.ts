import { UserModel } from './user.model';
import { OverviewModel } from './overview.model';
import { StoreModel } from './store.model';

export class ExpenseModel {
  _id: string;
  amount: number;
  date: Date;
  overviewRef: OverviewModel;
  userRef: UserModel;
  storeRef: StoreModel;

  constructor(values = {}) {
    Object.assign(this, values)
  }
}
