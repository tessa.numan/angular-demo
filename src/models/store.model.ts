import { AddressModel } from './address.model';

export class StoreModel {
  _id: string;
  name: string;
  emailAddress: string;
  phoneNumber: string;
  address: AddressModel;

  constructor(values = {}) {
    Object.assign(this, values)
  }
}
