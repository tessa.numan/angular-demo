export class AddressModel {
  _id: string;
  city: string;
  street: string;
  houseNumber: string;
  postalCode: string;

  constructor(values = {}) {
    Object.assign(this, values)
  }
}
