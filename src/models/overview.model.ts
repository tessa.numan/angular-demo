import { UserModel } from './user.model';
import { ExpenseModel } from './expense.model';

export class OverviewModel {
  _id: string;
  name: number;
  description: string;
  year: number;
  userRefs: UserModel[];
  expenseRefs: ExpenseModel[];

  constructor(values = {}) {
    Object.assign(this, values)
  }
}
