import { Component, Input, OnInit } from '@angular/core';
import { AuthService } from 'src/services/auth.service'

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit {
  @Input() title: string;
  isNavbarCollapsed = true;

  constructor(public authService: AuthService) {}

  ngOnInit(): void {}
}
