import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { FooterComponent } from './shared/footer/footer.component';
import { LayoutComponent } from './layout/layout.component';
import { UserDetailComponent } from './pages/user/user-detail/user-detail.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { LoginComponent } from './pages/user/login/login.component';
import { RegisterComponent } from './pages/user/register/register.component';
import { UsecasesComponent } from './pages/about/usecases/usecases.component';
import { UsecaseComponent } from './pages/about/usecases/usecase/usecase.component';
import { StoreDetailComponent } from './pages/store/store-detail/store-detail.component';
import { StoreListComponent } from './pages/store/store-list/store-list.component';
import { StoreEditComponent } from './pages/store/store-edit/store-edit.component';
import { StoreAddComponent } from './pages/store/store-add/store-add.component';
import { OverviewListComponent } from './pages/overview/overview-list/overview-list.component';
import { OverviewAddComponent } from './pages/overview/overview-add/overview-add.component';
import { OverviewDetailComponent } from './pages/overview/overview-detail/overview-detail.component';
import { OverviewEditComponent } from './pages/overview/overview-edit/overview-edit.component';
import { OverviewDetailUsersComponent } from './pages/overview/overview-detail-users/overview-detail-users.component';
import { ExpenseAddComponent } from './pages/expense/expense-add/expense-add.component';
import { ExpenseDetailComponent } from './pages/expense/expense-detail/expense-detail.component';
import { UserComponent } from './pages/user/user/user.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    LayoutComponent,
    UserDetailComponent,
    DashboardComponent,
    UsecasesComponent,
    UsecaseComponent,
    LoginComponent,
    RegisterComponent,
    StoreDetailComponent,
    StoreListComponent,
    StoreEditComponent,
    StoreAddComponent,
    OverviewListComponent,
    OverviewAddComponent,
    OverviewDetailComponent,
    OverviewEditComponent,
    OverviewDetailUsersComponent,
    ExpenseAddComponent,
    ExpenseDetailComponent,
    UserComponent
  ],
  imports: [BrowserModule, AppRoutingModule, NgbModule, HttpClientModule, FormsModule, ReactiveFormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
