import { Component, OnInit, Input } from '@angular/core';
import { ExpenseModel } from '../../../../models/expense.model';
import { ExpenseService } from '../../../../services/expense.service';
import { AuthService } from 'src/services/auth.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { UserService } from 'src/services/user.service';
import { UserModel } from 'src/models/user.model';

@Component({
  selector: 'app-expense-detail',
  templateUrl: './expense-detail.component.html',
  styleUrls: ['./expense-detail.component.css'],
})
export class ExpenseDetailComponent implements OnInit {
  expense: ExpenseModel;
  user: UserModel;
  correctUser: boolean;

  constructor(
    private expenseService: ExpenseService,
    private userService: UserService,
    private authService: AuthService,
    private route: ActivatedRoute,
    private location: Location
  ) {}

  ngOnInit(): void {
    if (this.loginCheck() === true) {
      this.correctUser = false;
      this.getExpense();
    }
  }

  getExpense(): void {
    const id = this.route.snapshot.paramMap.get('expenseId');
    this.expenseService.getExpense(id).subscribe((expense) => {
      this.expense = expense[0];
      this.userCheck();
      return this.expense[0];
    });
  }

  loginCheck() {
    const loggedIn = this.authService.isLoggedIn();
    if (loggedIn === null || loggedIn === undefined || loggedIn !== true) {
      return false;
    }
    return true;
  }

  userCheck() {
    this.userService.getUser().subscribe((u) => {
      if (u._id === this.expense.userRef._id) {
        this.correctUser = true;
      } else {
        this.correctUser = false;
      }
    });
  }

  delete(expense: ExpenseModel): void {
    if (!this.loginCheck()) {
      return;
    }
    this.expenseService.deleteExpense(expense).subscribe(() => this.goBack());
  }

  goBack(): void {
    this.location.back();
  }
}
