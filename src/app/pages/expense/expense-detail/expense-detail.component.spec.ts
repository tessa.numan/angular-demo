import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, convertToParamMap, Router } from '@angular/router';
import { ExpenseService } from '../../../../services/expense.service';
import { AuthService } from 'src/services/auth.service';
import { ExpenseDetailComponent } from './expense-detail.component';
import { UserService } from 'src/services/user.service';

describe('ExpenseDetailComponent', () => {
  let component: ExpenseDetailComponent;
  let fixture: ComponentFixture<ExpenseDetailComponent>;

  let expenseServiceSpy;
  let authServiceSpy;
  let userServiceSpy;
  let locationSpy;

  beforeEach(() => {
    authServiceSpy = jasmine.createSpyObj('AuthService', [
      'login',
      'logout',
      'isLoggedIn',
    ]);

    userServiceSpy = jasmine.createSpyObj('UserService', ['getUser']);

    expenseServiceSpy = jasmine.createSpyObj('ExpenseService', ['getExpenses']);

    TestBed.configureTestingModule({
      declarations: [ExpenseDetailComponent],
      providers: [
        { provide: ExpenseService, useValue: expenseServiceSpy },
        { provide: UserService, useValue: userServiceSpy },
        { provide: AuthService, useValue: authServiceSpy },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: { paramMap: convertToParamMap({ expenseId: '1' }) },
          },
        },
        { provide: Location, useValue: locationSpy },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(ExpenseDetailComponent);
    component = fixture.componentInstance;
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
