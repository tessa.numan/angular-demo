import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { AuthService } from 'src/services/auth.service';
import { OverviewService } from 'src/services/overview.service';
import { StoreService } from 'src/services/store.service';
import { UserService } from 'src/services/user.service';
import { ExpenseService } from '../../../../services/expense.service';
import { ExpenseAddComponent } from './expense-add.component';

describe('ExpenseAddComponent', () => {
  let component: ExpenseAddComponent;
  let fixture: ComponentFixture<ExpenseAddComponent>;

  let expenseServiceSpy;
  let storeServiceSpy;
  let overviewServiceSpy;
  let userServiceSpy;
  let authServiceSpy;
  let locationSpy;

  beforeEach(() => {
    authServiceSpy = jasmine.createSpyObj('AuthService', [
      'login',
      'logout',
      'isLoggedIn',
    ]);

    expenseServiceSpy = jasmine.createSpyObj('ExpenseService', ['getExpenses']);
    storeServiceSpy = jasmine.createSpyObj('StoreService', ['getStores']);
    overviewServiceSpy = jasmine.createSpyObj('OverviewService', [
      'getOverviews',
    ]);
    userServiceSpy = jasmine.createSpyObj('UserService', ['getUsers']);

    TestBed.configureTestingModule({
      declarations: [ExpenseAddComponent],
      imports: [FormsModule],
      providers: [
        { provide: ExpenseService, useValue: expenseServiceSpy },
        { provide: StoreService, useValue: storeServiceSpy },
        { provide: OverviewService, useValue: overviewServiceSpy },
        { provide: UserService, useValue: userServiceSpy },
        { provide: AuthService, useValue: authServiceSpy },
        { provide: Location, useValue: locationSpy },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: { paramMap: convertToParamMap({ expenseId: '1' }) },
          },
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(ExpenseAddComponent);
    component = fixture.componentInstance;
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
