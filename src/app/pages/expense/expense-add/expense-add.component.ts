import { Component, OnInit } from '@angular/core';
import { ExpenseModel } from 'src/models/expense.model';
import { ExpenseService } from 'src/services/expense.service';
import { OverviewService } from 'src/services/overview.service';
import { UserModel } from 'src/models/user.model';
import { OverviewModel } from 'src/models/overview.model';
import { UserService } from 'src/services/user.service';
import { StoreService } from 'src/services/store.service';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { StoreModel } from 'src/models/store.model';
import { AuthService } from 'src/services/auth.service';

@Component({
  selector: 'app-expense-add',
  templateUrl: './expense-add.component.html',
  styleUrls: ['./expense-add.component.css'],
})
export class ExpenseAddComponent implements OnInit {
  expense: ExpenseModel;
  userRef: UserModel;
  overview: OverviewModel;
  dropdownArray: StoreModel[];
  storeRef: StoreModel;

  constructor(
    private expenseService: ExpenseService,
    private authService: AuthService,
    private storeService: StoreService,
    private overviewService: OverviewService,
    private userService: UserService,
    private location: Location,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    if (this.loginCheck() === true) {
      this.getOverview();
      this.getStores();
      this.expense = new ExpenseModel();
      this.userService.getUser().subscribe((user) => {
        this.userRef = user;
      });
    }
  }

  getOverview(): void {
    const id = this.route.snapshot.paramMap.get('overviewId');
    this.overviewService.getOverview(id).subscribe((overview) => {
      this.overview = overview[0];
      return this.overview[0];
    });
  }

  getStores(): void {
    this.storeService.getStores().subscribe((stores) => {
      this.dropdownArray = stores;
    });
  }

  onSubmit(): void {
    let year = new Date(this.expense.date);

    if (this.storeRef) {
      this.expense.storeRef = this.storeRef;
    }

    if (year.getFullYear() !== this.overview.year) {
      alert(
        'De opgegeven datum komt niet overeen met het jaar van het overzicht'
      );
      return;
    }

    if (this.userRef && this.overview) {
      this.expense.userRef = this.userRef;
      this.expense.overviewRef = this.overview;
      this.expenseService
        .addExpense(this.expense)
        .subscribe(() => this.goBack());
    } else {
      console.log(
        'Er is iets fout gegaan bij het toevoegen van de user en/of de overview aan de expense'
      );
      return;
    }
  }

  loginCheck() {
    const loggedIn = this.authService.isLoggedIn();
    if (loggedIn === null || loggedIn === undefined || loggedIn !== true) {
      return false;
    }
    return true;
  }

  goBack(): void {
    this.location.back();
  }
}
