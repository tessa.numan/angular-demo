import { Component, OnInit } from '@angular/core';
import { OverviewModel } from 'src/models/overview.model';
import { OverviewService } from 'src/services/overview.service';
import { UserModel } from 'src/models/user.model';
import { UserService } from 'src/services/user.service';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/services/auth.service';

@Component({
  selector: 'app-overview-edit',
  templateUrl: './overview-edit.component.html',
  styleUrls: ['./overview-edit.component.css'],
})
export class OverviewEditComponent implements OnInit {
  overview: OverviewModel;

  constructor(
    private overviewService: OverviewService,
    private location: Location,
    private route: ActivatedRoute,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    if (this.loginCheck() === true) {
      this.getOverview();
    }
  }

  getOverview(): void {
    const id = this.route.snapshot.paramMap.get('overviewId');
    this.overviewService.getOverview(id).subscribe((overview) => {
      this.overview = overview[0];
      return this.overview[0];
    });
  }

  onSubmit(): void {
    this.overviewService
      .updateOverview(this.overview)
      .subscribe(() => this.goBack());
  }

  loginCheck() {
    const loggedIn = this.authService.isLoggedIn();
    if (loggedIn === null || loggedIn === undefined || loggedIn !== true) {
      return false;
    }
    return true;
  }

  goBack(): void {
    this.location.back();
  }
}
