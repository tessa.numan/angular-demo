import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { BehaviorSubject, of } from 'rxjs';
import { FormsModule } from '@angular/forms';
import { OverviewModel } from '../../../../models/overview.model';
import { OverviewService } from '../../../../services/overview.service';
import { AuthService } from '../../../../services/auth.service';
import { Location } from '@angular/common';
import { UserModel } from 'src/models/user.model';
import { OverviewEditComponent } from './overview-edit.component';

describe('OverviewEditComponent', () => {
  let component: OverviewEditComponent;
  let fixture: ComponentFixture<OverviewEditComponent>;

  let overviewServiceSpy;
  let authServiceSpy;
  let locationSpy;

  beforeEach(() => {
    authServiceSpy = jasmine.createSpyObj('AuthService', [
      'login',
      'logout',
      'isLoggedIn',
    ]);

    overviewServiceSpy = jasmine.createSpyObj('OverviewService', [
      'getOverview',
      'updateOverview',
    ]);

    TestBed.configureTestingModule({
      declarations: [OverviewEditComponent],
      imports: [FormsModule],

      providers: [
        { provide: OverviewService, useValue: overviewServiceSpy },
        { provide: AuthService, useValue: authServiceSpy },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: { paramMap: convertToParamMap({ overviewId: '1' }) },
          },
        },
        { provide: Location, useValue: locationSpy },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(OverviewEditComponent);
    component = fixture.componentInstance;
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
