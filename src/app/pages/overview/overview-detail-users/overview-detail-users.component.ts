import { Component, OnInit } from '@angular/core';
import { OverviewModel } from 'src/models/overview.model';
import { OverviewService } from 'src/services/overview.service';
import { UserModel } from 'src/models/user.model';
import { UserService } from 'src/services/user.service';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/services/auth.service';

@Component({
  selector: 'app-overview-detail-users',
  templateUrl: './overview-detail-users.component.html',
  styleUrls: ['./overview-detail-users.component.css'],
})
export class OverviewDetailUsersComponent implements OnInit {
  overview: OverviewModel;
  dropdownArray: UserModel[];
  newUser: UserModel;
  updatedArray: UserModel[];
  addBool: boolean;

  constructor(
    private overviewService: OverviewService,
    private userService: UserService,
    private authService: AuthService,
    private location: Location,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    if (this.loginCheck() === true) {
      this.addBool = false;
      this.getOverview();
    }
  }

  getOverview(): void {
    const id = this.route.snapshot.paramMap.get('overviewId');
    this.overviewService.getOverview(id).subscribe((overview) => {
      this.overview = overview[0];
      return this.overview[0];
    });
  }

  getUsers(): void {
    let idArray = this.overview.userRefs.map((r) => {
      return r._id;
    });

    this.userService.getUsers().subscribe((users) => {
      this.dropdownArray = users.filter((u) => !idArray.includes(u._id));
    });
    this.addBool = true;
  }

  onSubmit(): void {
    if (this.newUser) {
      this.overview.userRefs.push(this.newUser);
      this.overviewService
        .updateOverview(this.overview)
        .subscribe(() => this.goBack());
    } else {
      alert('Geef gebruiker op');
    }
  }

  deleteUser(deletedUser: UserModel): void {
    let userArray = this.overview.userRefs.filter(
      (u) => u._id !== deletedUser._id
    );

    this.overviewService
      .updateOverviewUsers(this.overview, userArray)
      .subscribe();
  }

  loginCheck() {
    const loggedIn = this.authService.isLoggedIn();
    if (loggedIn === null || loggedIn === undefined || loggedIn !== true) {
      return false;
    }
    return true;
  }

  goBack(): void {
    this.location.back();
  }
}
