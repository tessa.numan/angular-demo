import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, convertToParamMap, Router } from '@angular/router';
import { OverviewService } from '../../../../services/overview.service';
import { UserService } from '../../../../services/user.service';
import { AuthService } from 'src/services/auth.service';
import { OverviewDetailUsersComponent } from './overview-detail-users.component';

describe('OverviewDetailUsersComponent', () => {
  let component: OverviewDetailUsersComponent;
  let fixture: ComponentFixture<OverviewDetailUsersComponent>;

  let overviewServiceSpy;
  let userServiceSpy;
  let locationSpy;
  let authServiceSpy;

  beforeEach(() => {
    userServiceSpy = jasmine.createSpyObj('UserService', ['getUsers']);
    authServiceSpy = jasmine.createSpyObj('AuthService', [
      'login',
      'logout',
      'isLoggedIn',
    ]);

    overviewServiceSpy = jasmine.createSpyObj('OverviewService', [
      'getOverviews',
    ]);

    TestBed.configureTestingModule({
      declarations: [OverviewDetailUsersComponent],
      providers: [
        { provide: OverviewService, useValue: overviewServiceSpy },
        { provide: UserService, useValue: userServiceSpy },
        { provide: AuthService, useValue: authServiceSpy },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: { paramMap: convertToParamMap({ overviewId: '1' }) },
          },
        },
        { provide: Location, useValue: locationSpy },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(OverviewDetailUsersComponent);
    component = fixture.componentInstance;
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
