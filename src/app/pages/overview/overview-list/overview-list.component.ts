import { Component, OnInit } from '@angular/core';
import { OverviewModel } from '../../../../models/overview.model';
import { OverviewService } from '../../../../services/overview.service';
import { AuthService } from 'src/services/auth.service';

@Component({
  selector: 'app-overview-list',
  templateUrl: './overview-list.component.html',
  styleUrls: ['./overview-list.component.css'],
})
export class OverviewListComponent implements OnInit {
  overviewsArray: OverviewModel[];

  constructor(
    private overviewService: OverviewService,
    private authService: AuthService
  ) {}

  ngOnInit() {
    if (this.loginCheck() === true) {
      this.overviewsArray = new Array();
      this.getOverviews();
    }
  }

  getOverviews(): void {
    this.overviewService.getOverviews().subscribe((overviews) => {
      this.overviewsArray = overviews;
    });
  }

  loginCheck() {
    const loggedIn = this.authService.isLoggedIn();
    if (loggedIn === null || loggedIn === undefined || loggedIn !== true) {
      return false;
    }
    return true;
  }
}
