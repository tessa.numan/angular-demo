import { ComponentFixture, TestBed } from '@angular/core/testing';
import { OverviewService } from '../../../../services/overview.service';
import { AuthService } from 'src/services/auth.service';
import { OverviewListComponent } from './overview-list.component';

describe('OverviewListComponent', () => {
  let component: OverviewListComponent;
  let fixture: ComponentFixture<OverviewListComponent>;

  let overviewServiceSpy;
  let authServiceSpy;

  beforeEach(() => {
    authServiceSpy = jasmine.createSpyObj('AuthService', [
      'login',
      'logout',
      'isLoggedIn',
    ]);

    overviewServiceSpy = jasmine.createSpyObj('OverviewService', [
      'getOverviews',
    ]);

    TestBed.configureTestingModule({
      declarations: [OverviewListComponent],
      providers: [
        { provide: OverviewService, useValue: overviewServiceSpy },
        { provide: AuthService, useValue: authServiceSpy },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(OverviewListComponent);
    component = fixture.componentInstance;
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
