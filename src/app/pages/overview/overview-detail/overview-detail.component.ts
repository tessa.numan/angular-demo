import { Component, OnInit, Input } from '@angular/core';
import { OverviewModel } from '../../../../models/overview.model';
import { ExpenseModel } from '../../../../models/expense.model';
import { OverviewService } from '../../../../services/overview.service';
import { AuthService } from 'src/services/auth.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { UserService } from 'src/services/user.service';
import { UserModel } from 'src/models/user.model';

@Component({
  selector: 'app-overview-detail',
  templateUrl: './overview-detail.component.html',
  styleUrls: ['./overview-detail.component.css'],
})
export class OverviewDetailComponent implements OnInit {
  overview: OverviewModel;
  totalAmount: number;
  correctUser: boolean;

  constructor(
    private overviewService: OverviewService,
    private userService: UserService,
    private authService: AuthService,
    private route: ActivatedRoute,
    private location: Location
  ) {}

  ngOnInit(): void {
    if (this.loginCheck() === true) {
      this.correctUser = false;
      this.totalAmount = 0;
      this.getOverview();
    }
  }

  getOverview(): void {
    const id = this.route.snapshot.paramMap.get('overviewId');
    this.overviewService.getOverview(id).subscribe((overview) => {
      this.overview = overview[0];
      this.getTotal();
      this.userCheck();
      return this.overview[0];
    });
  }

  userCheck() {
    this.userService.getUser().subscribe((u) => {
      this.overview.userRefs.forEach((user) => {
        if (u._id === user._id) {
          this.correctUser = true;
        }
      });
    });
  }

  getTotal() {
    this.overview.expenseRefs.forEach((e) => {
      this.totalAmount += e.amount;
    });
  }

  getTotalByUser(user: string) {
    let total = 0;
    this.overview.expenseRefs.forEach((e) => {
      if (e.userRef.toString() == user) {
        total += e.amount;
      }
    });

    return total;
  }

  loginCheck() {
    const loggedIn = this.authService.isLoggedIn();
    if (loggedIn === null || loggedIn === undefined || loggedIn !== true) {
      return false;
    }
    return true;
  }

  delete(overview: OverviewModel): void {
    if (!this.loginCheck()) {
      return;
    }
    this.overviewService
      .deleteOverview(overview)
      .subscribe(() => this.goBack());
  }

  goBack(): void {
    this.location.back();
  }
}
