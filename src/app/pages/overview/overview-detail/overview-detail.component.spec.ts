import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, convertToParamMap, Router } from '@angular/router';
import { OverviewService } from '../../../../services/overview.service';
import { AuthService } from 'src/services/auth.service';
import { OverviewDetailComponent } from './overview-detail.component';
import { UserService } from 'src/services/user.service';

describe('OverviewDetailComponent', () => {
  let component: OverviewDetailComponent;
  let fixture: ComponentFixture<OverviewDetailComponent>;

  let overviewServiceSpy;
  let authServiceSpy;
  let locationSpy;
  let userServiceSpy;

  beforeEach(() => {
    authServiceSpy = jasmine.createSpyObj('AuthService', [
      'login',
      'logout',
      'isLoggedIn',
    ]);

    overviewServiceSpy = jasmine.createSpyObj('OverviewService', [
      'getOverviews',
    ]);
    overviewServiceSpy = jasmine.createSpyObj('OverviewService', ['getUsers']);

    TestBed.configureTestingModule({
      declarations: [OverviewDetailComponent],
      providers: [
        { provide: OverviewService, useValue: overviewServiceSpy },
        { provide: AuthService, useValue: authServiceSpy },
        { provide: UserService, useValue: userServiceSpy },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: { paramMap: convertToParamMap({ overviewId: '1' }) },
          },
        },
        { provide: Location, useValue: locationSpy },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(OverviewDetailComponent);
    component = fixture.componentInstance;
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
