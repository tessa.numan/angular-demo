import { Component, OnInit } from '@angular/core';
import { OverviewModel } from 'src/models/overview.model';
import { OverviewService } from 'src/services/overview.service';
import { UserModel } from 'src/models/user.model';
import { UserService } from 'src/services/user.service';
import { Location } from '@angular/common';
import { AuthService } from 'src/services/auth.service';

@Component({
  selector: 'app-overview-add',
  templateUrl: './overview-add.component.html',
  styleUrls: ['./overview-add.component.css'],
})
export class OverviewAddComponent implements OnInit {
  overview: OverviewModel;
  userRef: UserModel;
  boolSubmit: boolean;

  constructor(
    private overviewService: OverviewService,
    private userService: UserService,
    private authService: AuthService,
    private location: Location
  ) {}

  ngOnInit(): void {
    if (this.loginCheck() === true) {
      this.overview = new OverviewModel();
      this.boolSubmit = false;
      this.userService.getUser().subscribe((user) => {
        this.userRef = user;
      });
    }
  }

  onSubmit(): void {
    if (this.userRef) {
      this.overview.userRefs = [this.userRef];
      this.overviewService
        .addOverview(this.overview)
        .subscribe(() => this.goBack());
    } else {
      console.log(
        'Er is iets fout gegaan bij het toevoegen van de user aan de overview'
      );
      return;
    }
  }

  loginCheck() {
    const loggedIn = this.authService.isLoggedIn();
    if (loggedIn === null || loggedIn === undefined || loggedIn !== true) {
      return false;
    }
    return true;
  }

  goBack(): void {
    this.location.back();
  }
}
