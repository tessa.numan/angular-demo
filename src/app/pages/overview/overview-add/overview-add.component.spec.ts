import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { AuthService } from 'src/services/auth.service';
import { UserService } from 'src/services/user.service';
import { OverviewService } from '../../../../services/overview.service';
import { OverviewAddComponent } from './overview-add.component';

describe('OverviewAddComponent', () => {
  let component: OverviewAddComponent;
  let fixture: ComponentFixture<OverviewAddComponent>;

  let overviewServiceSpy;
  let userServiceSpy;
  let locationSpy;
  let authServiceSpy;

  beforeEach(() => {
    authServiceSpy = jasmine.createSpyObj('AuthService', [
      'login',
      'logout',
      'isLoggedIn',
    ]);

    overviewServiceSpy = jasmine.createSpyObj('OverviewService', [
      'getOverviews',
    ]);

    userServiceSpy = jasmine.createSpyObj('UserService', ['getUsers']);

    TestBed.configureTestingModule({
      declarations: [OverviewAddComponent],
      imports: [FormsModule],
      providers: [
        { provide: OverviewService, useValue: overviewServiceSpy },
        { provide: AuthService, useValue: authServiceSpy },
        { provide: Location, useValue: locationSpy },
        { provide: UserService, useValue: userServiceSpy },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(OverviewAddComponent);
    component = fixture.componentInstance;
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
