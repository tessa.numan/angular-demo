import { Component, OnInit } from '@angular/core';
import { StoreModel } from 'src/models/store.model';
import { AddressModel } from 'src/models/address.model';
import { StoreService } from 'src/services/store.service';
import { Location } from '@angular/common';
import { AuthService } from 'src/services/auth.service';

@Component({
  selector: 'app-store-add',
  templateUrl: './store-add.component.html',
  styleUrls: ['./store-add.component.css'],
})
export class StoreAddComponent implements OnInit {
  store: StoreModel;
  address: AddressModel;
  boolSubmit: boolean;

  constructor(
    private storeService: StoreService,
    private authService: AuthService,
    private location: Location
  ) {}

  ngOnInit(): void {
    if (this.loginCheck() === true) {
      this.address = new AddressModel();
      this.store = new StoreModel();
      this.boolSubmit = false;
    }
  }

  onSubmit(): void {
    this.store.address = this.address;
    this.storeService.addStore(this.store).subscribe(() => this.goBack());
  }

  goBack(): void {
    this.location.back();
  }

  loginCheck() {
    const loggedIn = this.authService.isLoggedIn();
    if (loggedIn === null || loggedIn === undefined || loggedIn !== true) {
      return false;
    }
    return true;
  }
}
