import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { AuthService } from 'src/services/auth.service';
import { StoreService } from '../../../../services/store.service';
import { StoreAddComponent } from './store-add.component';

describe('StoreAddComponent', () => {
  let component: StoreAddComponent;
  let fixture: ComponentFixture<StoreAddComponent>;

  let storeServiceSpy;
  let authServiceSpy;
  let locationSpy;

  beforeEach(() => {
    authServiceSpy = jasmine.createSpyObj('AuthService', [
      'login',
      'logout',
      'isLoggedIn',
    ]);

    storeServiceSpy = jasmine.createSpyObj('StoreService', ['getStores']);

    TestBed.configureTestingModule({
      declarations: [StoreAddComponent],
      imports: [FormsModule],
      providers: [
        { provide: StoreService, useValue: storeServiceSpy },
        { provide: Location, useValue: locationSpy },
        { provide: AuthService, useValue: authServiceSpy },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(StoreAddComponent);
    component = fixture.componentInstance;
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
