import { Component, OnInit, Input } from '@angular/core';
import { StoreModel } from '../../../../models/store.model';
import { StoreService } from '../../../../services/store.service';
import { AuthService } from 'src/services/auth.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-store-edit',
  templateUrl: './store-edit.component.html',
  styleUrls: ['./store-edit.component.css'],
})
export class StoreEditComponent implements OnInit {
  store: StoreModel;

  constructor(
    private storeService: StoreService,
    private authService: AuthService,
    private route: ActivatedRoute,
    private location: Location
  ) {}

  ngOnInit(): void {
    if (this.authService.isLoggedIn()) {
      this.getStore();
    }
  }

  getStore(): void {
    const id = this.route.snapshot.paramMap.get('storeId');
    this.storeService.getStore(id).subscribe((store) => {
      this.store = store[0];
      return this.store[0];
    });
  }

  onSubmit(): void {
    this.storeService.updateStore(this.store).subscribe(() => this.goBack());
  }

  loginCheck() {
    const loggedIn = this.authService.isLoggedIn();
    if (loggedIn === null || loggedIn === undefined || loggedIn !== true) {
      return false;
    }
    return true;
  }

  goBack(): void {
    this.location.back();
  }
}
