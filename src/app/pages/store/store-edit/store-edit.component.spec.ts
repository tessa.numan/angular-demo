import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { BehaviorSubject, of } from 'rxjs';
import { FormsModule } from '@angular/forms';
import { StoreModel } from '../../../../models/store.model';
import { StoreService } from '../../../../services/store.service';
import { AuthService } from '../../../../services/auth.service';
import { Location } from '@angular/common';
import { UserModel } from 'src/models/user.model';
import { StoreEditComponent } from './store-edit.component';

const expectedStore: StoreModel = {
  _id: '1',
  name: 'name',
  emailAddress: 'name@hotmail.com',
  phoneNumber: '0612345678',
  address: {
    _id: '1',
    city: 'city',
    street: 'street',
    houseNumber: '50',
    postalCode: '1234AB',
  },
};

const userData: UserModel = {
  _id: '1',
  username: 'username',
  password: 'secret',
  newPassword: 'newSecret',
  emailAddress: 'name@hotmail.com',
  phoneNumber: '0612345678',
  address: {
    _id: '1',
    city: 'city',
    street: 'street',
    houseNumber: '50',
    postalCode: '1234AB',
  },
};

describe('StoreEditComponent', () => {
  let component: StoreEditComponent;
  let fixture: ComponentFixture<StoreEditComponent>;

  let storeServiceSpy;
  let authServiceSpy;
  let locationSpy;

  beforeEach(() => {
    authServiceSpy = jasmine.createSpyObj('AuthService', [
      'login',
      'logout',
      'isLoggedIn',
    ]);

    storeServiceSpy = jasmine.createSpyObj('StoreService', [
      'getStore',
      'updateStore',
    ]);

    TestBed.configureTestingModule({
      declarations: [StoreEditComponent],
      imports: [FormsModule],

      providers: [
        { provide: StoreService, useValue: storeServiceSpy },
        { provide: AuthService, useValue: authServiceSpy },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: { paramMap: convertToParamMap({ storeId: '1' }) },
          },
        },
        { provide: Location, useValue: locationSpy },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(StoreEditComponent);
    component = fixture.componentInstance;
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call getStore() once at ngOnInit', (done) => {
    authServiceSpy.isLoggedIn.and.returnValue(of(true));
    storeServiceSpy.getStore.and.returnValue(of([expectedStore]));

    fixture.detectChanges();
    expect(component).toBeTruthy();

    setTimeout(() => {
      expect(storeServiceSpy.getStore).toHaveBeenCalledTimes(1);
      done();
    }, 200);
  });

  it('should get store on ngOnInit', (done) => {
    authServiceSpy.isLoggedIn.and.returnValue(of(true));
    storeServiceSpy.getStore.and.returnValue(of([expectedStore]));

    fixture.detectChanges();
    expect(component).toBeTruthy();

    setTimeout(() => {
      expect(component.store).toEqual(expectedStore);
      done();
    }, 200);
  });
});
