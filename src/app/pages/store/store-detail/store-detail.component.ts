import { Component, OnInit, Input } from '@angular/core';
import { StoreModel } from '../../../../models/store.model';
import { StoreService } from '../../../../services/store.service';
import { AuthService } from 'src/services/auth.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-store-detail',
  templateUrl: './store-detail.component.html',
  styleUrls: ['./store-detail.component.css'],
})
export class StoreDetailComponent implements OnInit {
  store: StoreModel;

  constructor(
    private storeService: StoreService,
    private authService: AuthService,
    private route: ActivatedRoute,
    private location: Location
  ) {}

  ngOnInit(): void {
    if (this.loginCheck() === true) {
      this.getStore();
    }
  }

  getStore(): void {
    const id = this.route.snapshot.paramMap.get('storeId');
    this.storeService.getStore(id).subscribe((store) => {
      this.store = store[0];
      return this.store[0];
    });
  }

  loginCheck() {
    const loggedIn = this.authService.isLoggedIn();
    if (loggedIn === null || loggedIn === undefined || loggedIn !== true) {
      return false;
    }
    return true;
  }

  delete(store: StoreModel): void {
    if (!this.loginCheck()) {
      return;
    }
    this.storeService.deleteStore(store).subscribe(() => this.goBack());
  }

  goBack(): void {
    this.location.back();
  }
}
