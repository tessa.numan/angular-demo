import { ComponentFixture, TestBed } from '@angular/core/testing';
import { StoreService } from '../../../../services/store.service';
import { AuthService } from 'src/services/auth.service';
import { StoreListComponent } from './store-list.component';

describe('StoreListComponent', () => {
  let component: StoreListComponent;
  let fixture: ComponentFixture<StoreListComponent>;

  let storeServiceSpy;
  let authServiceSpy;

  beforeEach(() => {
    authServiceSpy = jasmine.createSpyObj('AuthService', [
      'login',
      'logout',
      'isLoggedIn',
    ]);

    storeServiceSpy = jasmine.createSpyObj('StoreService', ['getStores']);

    TestBed.configureTestingModule({
      declarations: [StoreListComponent],
      providers: [
        { provide: StoreService, useValue: storeServiceSpy },
        { provide: AuthService, useValue: authServiceSpy },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(StoreListComponent);
    component = fixture.componentInstance;
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
