import { Component, OnInit } from '@angular/core';
import { StoreModel } from '../../../../models/store.model';
import { StoreService } from '../../../../services/store.service';
import { AuthService } from 'src/services/auth.service';

@Component({
  selector: 'app-store-list',
  templateUrl: './store-list.component.html',
  styleUrls: ['./store-list.component.css'],
})
export class StoreListComponent implements OnInit {
  storesArray: StoreModel[];

  constructor(
    private storeService: StoreService,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.storesArray = new Array();
    this.getStores();
  }

  getStores(): void {
    this.storeService.getStores().subscribe((stores) => {
      this.storesArray = stores;
    });
  }

  loginCheck() {
    const loggedIn = this.authService.isLoggedIn();
    if (loggedIn === null || loggedIn === undefined || loggedIn !== true) {
      return false;
    }
    return true;
  }
}
