import { Component, OnInit, Input } from '@angular/core';
import { UserModel } from 'src/models/user.model';

import { Router } from '@angular/router';
import { AuthService } from 'src/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  user: UserModel;
  wrongCredentials: boolean;

  constructor(public authService: AuthService, private router: Router) {}

  ngOnInit(): void {
    this.user = new UserModel();
    this.wrongCredentials = false;
  }

  login(): void {
    this.authService.login(this.user);
    this.router.navigate(['/user']);
  }

  logout(): void {
    const loggedIn = this.authService.isLoggedIn();
    if (loggedIn === null || loggedIn === undefined || loggedIn !== true) {
      return;
    }
    this.authService.logout();
    this.router.navigate(['/user']);
  }
}
