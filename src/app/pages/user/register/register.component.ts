import { Component, OnInit } from '@angular/core';
import { UserModel } from 'src/models/user.model';
import { AddressModel } from 'src/models/address.model';
import { UserService } from 'src/services/user.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  user: UserModel;
  address: AddressModel;

  constructor(private userService: UserService, private location: Location) {}

  ngOnInit(): void {
    this.user = new UserModel();
    this.address = new AddressModel();
  }

  onSubmit(): void {
    if (!this.user.username || !this.user.password) {
      return;
    }
    this.user.address = this.address;
    this.user.username = this.user.username.trim();
    this.userService.addUser(this.user).subscribe(() => this.goBack());
  }

  goBack(): void {
    this.location.back();
  }
}
