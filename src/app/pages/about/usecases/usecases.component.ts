import { Component, OnInit } from '@angular/core';
import { UseCase } from '../usecase.model';

@Component({
  selector: 'app-about-usecases',
  templateUrl: './usecases.component.html',
  styleUrls: ['./usecases.component.css'],
})
export class UsecasesComponent implements OnInit {
  readonly PLAIN_USER = 'Reguliere gebruiker';
  readonly ADMIN_USER = 'Administrator';

  useCases: UseCase[] = [
    {
      id: 'UC-01',
      name: 'Inloggen',
      description: 'Hiermee logt een bestaande gebruiker in.',
      scenario: [
        'Gebruiker vult email en password in en klikt op Login knop.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.',
      ],
      actor: this.PLAIN_USER,
      precondition: 'Geen',
      postcondition: 'De actor is ingelogd',
    },
    {
      id: 'UC-02',
      name: 'Uitgave toevoegen',
      description: 'Hiermee kan een uitgave worden toegevoegd.',
      scenario: [
        'Gebruiker navigeert de pagina om een uitgaven toe te voegen.',
        'De applicatie toont het formulier om een uitgave toe te voegen.',
        'Gebruiker vult formulier in en selecteert een bestaande winkel.',
        'Gebruiker drukt op opslaan.',
        'De applicatie controleert ingevulde velden en slaat uitgave op.',
      ],
      actor: this.PLAIN_USER,
      precondition: 'De actor is ingelogd.',
      postcondition: 'Een nieuwe uitgave is toegevoegd.',
    },
    {
      id: 'UC-03',
      name: 'Uitgave details wijzigen',
      description:
        'Hiermee kunnen de details van een uitgave worden gewijzigd.',
      scenario: [
        'Gebruiker navigeert naar de lijstpagina met uitgaven.',
        'De applicatie toont de ingevoerde uitgaven.',
        'Gebruiker klikt een uitgave aan.',
        'De applicatie toont de details van de aangeklikte uitgave.',
        'Gebruiker drukt op de edit knop.',
        'De applicatie toont een formulier waar de uitgave kan worden aangepast.',
        'Gebruiker past de gegevens aan en drukt op opslaan.',
        'De applicatie toont de gewijzigde uitgave.',
      ],
      actor: this.PLAIN_USER,
      precondition: 'De actor is ingelogd.',
      postcondition: 'Uitgave is aangepast en wordt getoond.',
    },
    {
      id: 'UC-04',
      name: 'Uitgave verwijderen',
      description: 'Hiermee kan een uitgave worden verwijderd.',
      scenario: [
        'Gebruiker navigeert naar de lijstpagina met uitgaven.',
        'De applicatie toont de ingevoerde uitgaven.',
        'Gebruiker klikt een uitgave aan.',
        'De applicatie toont de details van de aangeklikte uitgave.',
        'Gebruiker drukt op de verwijder knop.',
        'De applicatie verwijderd de uitgave en toont de lijst met uitgaven.',
      ],
      actor: this.PLAIN_USER,
      precondition: 'De actor is ingelogd.',
      postcondition: 'De uitgave is verwijderd.',
    }

  ];

  constructor() {}

  ngOnInit(): void {}
}
