import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/user/login/login.component';
import { RegisterComponent } from './pages/user/register/register.component';
import { LayoutComponent } from './layout/layout.component';
import { UsecasesComponent } from './pages/about/usecases/usecases.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { UserDetailComponent } from './pages/user/user-detail/user-detail.component';
import { UserComponent } from './pages/user/user/user.component';
import { StoreDetailComponent } from './pages/store/store-detail/store-detail.component';
import { StoreListComponent } from './pages/store/store-list/store-list.component';
import { StoreEditComponent } from './pages/store/store-edit/store-edit.component';
import { StoreAddComponent } from './pages/store/store-add/store-add.component';
import { OverviewDetailComponent } from './pages/overview/overview-detail/overview-detail.component';
import { OverviewDetailUsersComponent } from './pages/overview/overview-detail-users/overview-detail-users.component';
import { OverviewListComponent } from './pages/overview/overview-list/overview-list.component';
import { OverviewEditComponent } from './pages/overview/overview-edit/overview-edit.component';
import { OverviewAddComponent } from './pages/overview/overview-add/overview-add.component';
import { ExpenseAddComponent } from './pages/expense/expense-add/expense-add.component';
import { ExpenseDetailComponent } from './pages/expense/expense-detail/expense-detail.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'dashboard' },
  { path: 'dashboard', pathMatch: 'full', component: DashboardComponent },
  { path: 'about', pathMatch: 'full', component: UsecasesComponent },
  { path: 'user', pathMatch: 'full', component: UserComponent },
  { path: 'user/:id', pathMatch: 'full', component: UserDetailComponent },
  { path: 'store', pathMatch: 'full', component: StoreListComponent },
  { path: 'store/add', pathMatch: 'full', component: StoreAddComponent },
  {
    path: 'store/:storeId',
    pathMatch: 'full',
    component: StoreDetailComponent,
  },
  {
    path: 'store/edit/:storeId',
    pathMatch: 'full',
    component: StoreEditComponent,
  },
  { path: 'overview', pathMatch: 'full', component: OverviewListComponent },
  { path: 'overview/add', pathMatch: 'full', component: OverviewAddComponent },
  {
    path: 'overview/:overviewId',
    pathMatch: 'full',
    component: OverviewDetailComponent,
  },
  {
    path: 'overview/users/:overviewId',
    pathMatch: 'full',
    component: OverviewDetailUsersComponent,
  },
  {
    path: 'expense/add/:overviewId',
    pathMatch: 'full',
    component: ExpenseAddComponent,
  },
  {
    path: 'expense/:expenseId',
    pathMatch: 'full',
    component: ExpenseDetailComponent,
  },

  { path: 'login', pathMatch: 'full', component: LoginComponent },
  { path: 'register', pathMatch: 'full', component: RegisterComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'dashboard' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
