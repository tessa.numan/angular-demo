import { by, element, ElementFinder } from 'protractor';
import { CommonPageObject } from '../../common.po';

export class StoreListPage extends CommonPageObject {

  get storesList(): ElementFinder {
    return element(by.id('storesList')) as ElementFinder;
  }

    get addButton(): ElementFinder {
    return element(by.id('add')) as ElementFinder;
  }

  get detailsButton(): ElementFinder {
    return element(by.id('details')) as ElementFinder;
  }

}
