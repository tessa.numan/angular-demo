import { StoreAddPage } from './add.po';
import { browser, logging, element, by } from 'protractor';
import { protractor } from 'protractor/built/ptor';

describe('Store add page', () => {
  let page: StoreAddPage;

  /**
   *
   */
  beforeEach(() => {
    page = new StoreAddPage();
  });

  /**
   *
   */
  it('should be at /store/add route after initialisation', () => {
    browser.waitForAngularEnabled(false);
    page.navigateTo('/store/add');
    expect(browser.getCurrentUrl()).toContain('/store/add');
  });

  /**
   *
   */
  it('should display error messages when no values are entered in inputs', () => {
    browser.waitForAngularEnabled(false);
    page.navigateTo('/store/add');
    // tslint:disable-next-line: quotemark
    browser.executeScript("window.localStorage.setItem('token', '123')");
    page.navigateTo('/store/add');

    page.nameInput.click();
    page.cityInput.click();

    page.nameInput.sendKeys(protractor.Key.chord(protractor.Key.CONTROL, 'a'));
    page.nameInput.sendKeys(protractor.Key.BACK_SPACE);
    page.cityInput.sendKeys(protractor.Key.chord(protractor.Key.CONTROL, 'a'));
    page.cityInput.sendKeys(protractor.Key.BACK_SPACE);

    browser.driver.sleep(100);

    expect(page.nameInvalidMessage).toBeTruthy();
    expect(page.nameInvalidMessage.getText()).toContain('Voer een naam in');
    expect(page.cityInvalidMessage).toBeTruthy();
    expect(page.cityInvalidMessage.getText()).toContain('Voer een stad in');

    expect(browser.getCurrentUrl()).toContain('localhost:4200/store/add');
  });

  /**
   * Promised approach
   */
  it('should add store successfully when name and city are entered', async () => {
    browser.waitForAngularEnabled(false);
    page.navigateTo('/store/add');
    // tslint:disable-next-line: quotemark
    browser.executeScript("window.localStorage.setItem('token', '123')");
    page.navigateTo('/store/add');

    page.nameInput.sendKeys('name');
    page.cityInput.sendKeys('city');
    expect(page.submitStoreButton.isEnabled()).toBe(true);
    expect(page.submitStoreButton).toBeTruthy();

    browser.executeScript('arguments[0].click();', page.submitStoreButton);

    browser.driver.sleep(1000);
    expect(browser.getCurrentUrl()).toContain('/store');
    expect(element(by.id('add'))).toBeTruthy();
  });

  /**
   *
   */
  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);

    expect(logs).not.toContain(
      jasmine.objectContaining({
        level: logging.Level.SEVERE,
      } as logging.Entry)
    );

    // tslint:disable-next-line: quotemark
    browser.executeScript("window.localStorage.removeItem('currentuser')");
  });
});
