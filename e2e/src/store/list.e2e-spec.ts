import { StoreListPage } from './list.po';
import { browser, logging, element, by } from 'protractor';
import { protractor } from 'protractor/built/ptor';


describe('Store list page', () => {
  let page: StoreListPage;

  /**
   *
   */
  beforeEach(() => {
    page = new StoreListPage();
  });

  /**
   *
   */
  it('should be at /store route after initialisation', () => {
    browser.waitForAngularEnabled(false);
    page.navigateTo('/store');
    expect(browser.getCurrentUrl()).toContain('/store');
  });

  /**
   *
   */
  it('should display a list of stores', () => {
    browser.waitForAngularEnabled(false);
    page.navigateTo('/store');

    expect(page.storesList).toBeTruthy();
    let cardCount = element.all(by.id('storeCard'))

    expect(cardCount.count()).toEqual(2);

    expect(browser.getCurrentUrl()).toContain('localhost:4200/store');
  });

  /**
   * Promised approach
   */
  it('should navigate to /store/add when clicked on add button', () => {
    browser.waitForAngularEnabled(false);
    page.navigateTo('/store');
    // tslint:disable-next-line: quotemark
    browser.executeScript("window.localStorage.setItem('token', '123')");
    page.navigateTo('/store');

    expect(page.addButton).toBeTruthy();
    page.addButton.click();

    expect(browser.getCurrentUrl()).toContain('localhost:4200/store/add');
  });


  it('should display details of store when clicked on details button', () => {
    browser.waitForAngularEnabled(false);
    page.navigateTo('/store');
    // tslint:disable-next-line: quotemark
    browser.executeScript("window.localStorage.setItem('token', '123')");
    page.navigateTo('/store');

    expect(page.detailsButton).toBeTruthy();

    page.detailsButton.click();

    expect(browser.getCurrentUrl()).toContain('localhost:4200/store/1');
  });

  /**
   *
   */
  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);

    expect(logs).not.toContain(
      jasmine.objectContaining({
        level: logging.Level.SEVERE,
      } as logging.Entry)
    );

    // tslint:disable-next-line: quotemark
    browser.executeScript("window.localStorage.removeItem('currentuser')");
  });
});
