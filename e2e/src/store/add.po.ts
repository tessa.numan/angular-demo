import { by, element, ElementFinder } from 'protractor';
import { CommonPageObject } from '../../common.po';

export class StoreAddPage extends CommonPageObject {
  get nameInput(): ElementFinder {
    return element(by.id('name')) as ElementFinder;
  }

  get cityInput(): ElementFinder {
    return element(by.id('city')) as ElementFinder;
  }

  get submitStoreButton(): ElementFinder {
    return element(by.id('submitStore')) as ElementFinder;
  }

  get nameInvalidMessage(): ElementFinder {
    return element(by.id('nameInvalidMessage')) as ElementFinder;
  }

  get cityInvalidMessage(): ElementFinder {
    return element(by.id('cityInvalidMessage')) as ElementFinder;
  }

}
