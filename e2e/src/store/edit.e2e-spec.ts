import { StoreEditPage } from './edit.po';
import { browser, logging, element, by } from 'protractor';
import { protractor } from 'protractor/built/ptor';

describe('Store edit page', () => {
  let page: StoreEditPage;

  /**
   *
   */
  beforeEach(() => {
    page = new StoreEditPage();
  });

  /**
   *
   */
  it('should be at /store/edit/1 route after initialisation', () => {
    browser.waitForAngularEnabled(false);
    page.navigateTo('/store/edit/1');
    expect(browser.getCurrentUrl()).toContain('/store/edit/1');
  });

  /**
   *
   */
  it('should display error messages when no values are entered in inputs', () => {
    browser.waitForAngularEnabled(false);
    page.navigateTo('/store/edit/1');
    // tslint:disable-next-line: quotemark
    browser.executeScript("window.localStorage.setItem('token', '123')");
    page.navigateTo('/store/edit/1');

    page.nameInput.click();
    page.cityInput.click();

    page.nameInput.sendKeys(protractor.Key.chord(protractor.Key.CONTROL, 'a'));
    page.nameInput.sendKeys(protractor.Key.BACK_SPACE);
    page.cityInput.sendKeys(protractor.Key.chord(protractor.Key.CONTROL, 'a'));
    page.cityInput.sendKeys(protractor.Key.BACK_SPACE);

    browser.driver.sleep(100);

    expect(page.nameInvalidMessage).toBeTruthy();
    expect(page.nameInvalidMessage.getText()).toContain('Voer een naam in');
    expect(page.cityInvalidMessage).toBeTruthy();
    expect(page.cityInvalidMessage.getText()).toContain('Voer een stad in');

    expect(browser.getCurrentUrl()).toContain('localhost:4200/store/edit/1');
  });

  /**
   * Promised approach
   */
  it('should edit store successfully when name and city are entered', async () => {
    browser.waitForAngularEnabled(false);
    page.navigateTo('/store/edit/1');
    // tslint:disable-next-line: quotemark
    browser.executeScript("window.localStorage.setItem('token', '123')");
    page.navigateTo('/store/edit/1');

    page.nameInput.sendKeys('name');
    page.cityInput.sendKeys('city');
    expect(page.submitStoreButton.isEnabled()).toBe(true);
    expect(page.submitStoreButton).toBeTruthy();

    browser.executeScript('arguments[0].click();', page.submitStoreButton);

    browser.driver.sleep(1000);
    expect(browser.getCurrentUrl()).toContain('/store');
    expect(element(by.id('edit'))).toBeTruthy();
  });

  /**
   *
   */
  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);

    expect(logs).not.toContain(
      jasmine.objectContaining({
        level: logging.Level.SEVERE,
      } as logging.Entry)
    );

    // tslint:disable-next-line: quotemark
    browser.executeScript("window.localStorage.removeItem('currentuser')");
  });
});
