import { LoginPage } from './login.po';
import { browser, logging, element, by } from 'protractor';
import { protractor } from 'protractor/built/ptor';

/**
 * https://www.youtube.com/watch?v=ympTE-bLYaU
 */
describe('Login page', () => {
  let page: LoginPage;

  /**
   *
   */
  beforeEach(() => {
    page = new LoginPage();
  });

  /**
   *
   */
  it('should be at /user route after initialisation', () => {
    browser.waitForAngularEnabled(false);
    page.navigateTo('/user');
    expect(browser.getCurrentUrl()).toContain('/user');
  });

  /**
   *
   */
  it('should display error messages when no values are entered in inputs', () => {
    browser.waitForAngularEnabled(false);
    page.navigateTo('/user');

    page.usernameInput.click();
    page.passwordInput.click();

    // https://stackoverflow.com/a/52782814/3471923
    page.usernameInput.sendKeys(protractor.Key.chord(protractor.Key.CONTROL, 'a'));
    page.usernameInput.sendKeys(protractor.Key.BACK_SPACE);
    page.passwordInput.sendKeys(
      protractor.Key.chord(protractor.Key.CONTROL, 'a')
    );
    page.passwordInput.sendKeys(protractor.Key.BACK_SPACE);

    browser.driver.sleep(100);

    expect(page.usernameInvalidMessage).toBeTruthy();
    expect(page.usernameInvalidMessage.getText()).toContain(
      'Voer gebruikersnaam in'
    );
    expect(page.passwordInvalidMessage).toBeTruthy();
    expect(page.passwordInvalidMessage.getText()).toContain(
      'Voer wachtwoord in'
    );

    expect(browser.getCurrentUrl()).toContain('localhost:4200/user');
  });

  /**
   * Promised approach
   */
  it('should login successfully when provided with a valid username and password', () => {
    browser.waitForAngularEnabled(false);
    page.navigateTo('/user');

    page.usernameInput.sendKeys('name');
    page.passwordInput.sendKeys('secret');
    expect(page.submitButton.isEnabled()).toBe(true);

    page.submitButton.click();

    browser.driver.sleep(1000);
    expect(browser.getCurrentUrl()).toContain('/user');
    expect(element(by.id('welcomeMessage'))).toBeTruthy();
    expect(element(by.id('welcomeMessage')).getText()).toContain(
      'Hallo name'
    );

    // tslint:disable-next-line: quotemark
    const getStoredUser = "return window.localStorage.getItem('currentuser');";
    browser.executeScript(getStoredUser).then((user) => {
      expect(user).toBeTruthy();
      expect(user).toContain('1');
    });
  });

  /**
   *
   */
  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);

    expect(logs).not.toContain(
      jasmine.objectContaining({
        level: logging.Level.SEVERE,
      } as logging.Entry)
    );

    // tslint:disable-next-line: quotemark
    browser.executeScript("window.localStorage.removeItem('currentuser')");
  });
});
