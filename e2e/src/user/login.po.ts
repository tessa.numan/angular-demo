import { by, element, ElementFinder } from 'protractor';
import { CommonPageObject } from '../../common.po';

export class LoginPage extends CommonPageObject {
  get usernameInput(): ElementFinder {
    return element(by.id('username')) as ElementFinder;
  }

  get passwordInput(): ElementFinder {
    return element(by.id('password')) as ElementFinder;
  }

  get submitButton(): ElementFinder {
    return element(by.id('submit')) as ElementFinder;
  }

  get usernameInvalidMessage(): ElementFinder {
    return element(by.id('usernameInvalidMessage')) as ElementFinder;
  }

  get passwordInvalidMessage(): ElementFinder {
    return element(by.id('passwordInvalidMessage')) as ElementFinder;
  }


}
